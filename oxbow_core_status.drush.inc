<?php

/**
 * Implements hook_drush_command().
 */
function oxbow_core_status_drush_command() {
  $items = array();

  $items['oxbow-core-status'] = array(
    'description' => 'Performs an update status check on Oxbow Core.',
    'aliases' => array('oxbow-status', 'ox-status', 'ocs'),
    'examples' => array(
      'drush ox-status' => 'Prints information on the latest release of Oxbow Core.',
    ),
  );

  return $items;
}

/**
 * Callback for the oxbow-core-status command
 */
function drush_oxbow_core_status() {
  $latest_tag = _oxbow_core_status_get_latest_tag();
  $profile_info = _oxbow_core_status_profile_info();
  $current_version = _oxbow_core_status_profile_version($profile_info['version']);

  $message = '';
  $check = '';
  if(!empty($latest_tag) && !empty($profile_info) && !empty($current_version)) {
    $check = _oxbow_core_status_check_for_update($current_version, $latest_tag);
  }

  if(!empty($check)) {
    echo "Current Version\t: " . $current_version . "\n";
    echo "Latest Release\t: " . $latest_tag . "\n";
    if($check != UPDATE_CURRENT) {
      $message = 'There is a new release available for Oxbow Core.';
    }
    else {
      $message = 'The installed version of Oxbow Core is up to date.';
    }
  }
  else {
    $message = 'There was a problem checking for updates for Oxbow Core. :(';
  }

  echo $message . "\n";
}
